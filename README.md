A Voice Detection class/program that trains an ensemble classifier to determine if a voice sample is male or female.

Requires pyQT5 (for gui), scikit-learn, and python3.

After cloning, run ``python gui.py`` in the src directory to demo.

Does not do well with non-cis samples.
[(Report)](report.pdf) is available for further  details.
