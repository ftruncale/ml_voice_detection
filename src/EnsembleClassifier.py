import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, VotingClassifier
from sklearn.svm import LinearSVC
from sklearn.preprocessing import LabelEncoder

class EnsembleClassifier:

    def __init__(self):
        dataset = pd.read_csv('../datasets/voice.csv', delim_whitespace = False) #comma separated list
        IA = dataset.iloc[:,:].values
        X = IA[:,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]] #These are various values from the audio samples, found in voice.csv
        y = IA[:,20] #Corresponding labels for the above audio samples
        le = LabelEncoder()
        y = le.fit_transform(y) #we can't throw something with strings in

        rtc = RandomForestClassifier(criterion="entropy") #entropy gives us a much better result
        abc = AdaBoostClassifier(n_estimators=1000)
        lsvc = LinearSVC()
        self.eclf = VotingClassifier(estimators=[('RandomForest', rtc), ('LinearSVC', lsvc), ('AdaBoost', abc)], voting='hard') #the ensemble classifier

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

        rtc.fit(X_train, y_train)
        lsvc.fit(X_train, y_train)
        abc.fit(X_train, y_train)
        self.eclf.fit(X_train, y_train)

    def predict(self, input):
        return self.eclf.predict(np.asarray(input).reshape((1,-1)))[0]
