import sys, os
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import pyqtSlot, Qt
from EnsembleClassifier import *

class VoiceApp(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Voice Detection Frontend'
        self.left = 300
        self.top = 300
        self.width = 360
        self.height = 400
        self.eclf = EnsembleClassifier() #the ensemble classifier, defined in machine.py
        self.initUI()

    def initUI(self):
        hbox = QHBoxLayout(self)

        topleft = QFrame(self)
        tl_hbox = QHBoxLayout(self)
        topleft.setFrameShape(QFrame.StyledPanel)
        topleft.setLayout(tl_hbox)

        topright = QFrame(self)
        tr_hbox = QHBoxLayout(self)
        topright.setFrameShape(QFrame.StyledPanel)
        topright.setLayout(tr_hbox)

        bottom = QFrame(self)
        bottom.setFrameShape(QFrame.StyledPanel)
        bottom_hbox = QHBoxLayout(self)
        bottom.setLayout(bottom_hbox)

        splitter1 = QSplitter(Qt.Horizontal)
        splitter1.addWidget(topleft)
        splitter1.addWidget(topright)

        splitter2 = QSplitter(Qt.Vertical)
        splitter2.addWidget(splitter1)
        splitter2.addWidget(bottom)


        button = QPushButton('Select File', self)
        button.setToolTip('File Select')
        button.move(100,70)
        button.clicked.connect(self.on_click)
        tl_hbox.addWidget(button)

        self.g_label = QLabel()
        self.g_label.setText("No data.")
        self.g_image = QLabel()

        tr_hbox.addWidget(self.g_label)
        tr_hbox.addWidget(self.g_image)

        self.result_table = QTableWidget()
        self.result_table.setColumnCount(3)
        self.result_table.setHorizontalHeaderLabels(["Filename", "Raw Result", "Determination"])
        bottom_hbox.addWidget(self.result_table)

        hbox.addWidget(splitter2)

        self.setLayout(hbox)
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()

    @pyqtSlot()
    def on_click(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filename, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","Wav Audio (*.wav)", options=options)
        if filename:
            check = self.eclf.predict(SpectrumAnalyze(filename))
            self.result_table.insertRow(self.result_table.rowCount())
            if check == 1:
                self.g_label.setText("Male ♂️")
                self.result_table.setItem(self.result_table.rowCount()-1, 2, QTableWidgetItem("Male ♂️"))
            else:
                self.g_label.setText("Female ♀️")
                self.result_table.setItem(self.result_table.rowCount()-1, 2, QTableWidgetItem("Female ♀️"))


            self.result_table.setItem(self.result_table.rowCount()-1, 0, QTableWidgetItem(os.path.basename(filename)))
            self.result_table.setItem(self.result_table.rowCount()-1, 1, QTableWidgetItem(str(check)))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = VoiceApp()
    sys.exit(app.exec_())
