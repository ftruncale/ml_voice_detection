from SpectrumAnalyze import *
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, VotingClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.preprocessing import LabelEncoder
import os
import csv
import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from pydotplus import graph_from_dot_data
from sklearn.tree import export_graphviz # want to show tree information in program
from seaborn import pairplot

dataset = pd.read_csv('../datasets/voice_alter.csv', delim_whitespace = False) #comma separated list
IA = dataset.iloc[:,:].values
X = IA[:,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]]
#X = IA[:,[0, 1, 5, 12]]
y = IA[:,20]
le = LabelEncoder()
y = le.fit_transform(y) #we can't throw something with strings in
rtc = RandomForestClassifier(criterion="entropy") #entropy gives us a much better result
abc = AdaBoostClassifier(n_estimators=1000)
svc = SVC(kernel="rbf")
lsvc = LinearSVC()
eclf = VotingClassifier(estimators=[('RandomForest', rtc), ('LinearSVC', lsvc), ('AdaBoost', abc)], voting='hard')
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)
dtc = DecisionTreeClassifier(criterion="entropy")
dtc.fit(X_train, y_train)
rtc.fit(X_train, y_train)
svc.fit(X_train, y_train)
abc.fit(X_train, y_train)
lsvc.fit(X_train, y_train)
eclf.fit(X_train, y_train)

#Processes audio files in the /samples/ directory, exports to /datasets/import.csv
def process_samples():
    rowstowrite = []
    for folder in os.listdir("../samples"):
        print(folder)
        for file in os.listdir("../samples/"+folder):
            print(file)
            buffer = SpectrumAnalyze("../samples/"+folder+"/"+file)
            buffer.append(folder)
            rowstowrite.append(buffer)

    with open('../datasets/import.csv', 'w+') as myfile:
        print(rowstowrite)
        wr = csv.writer(myfile)
        wr.writerows(rowstowrite)

#Writes an image to the /images folder containing the decision tree classifier graph
def generate_images():
    dataset = pd.read_csv('../datasets/voice.csv', delim_whitespace = False) #comma separated list
    # g=pairplot(dataset, hue="label", palette="husl").savefig("../images/test.png")
    dtc_data = export_graphviz(dtc, filled=True, rounded=True, feature_names=dataset.columns.tolist()[0:20], class_names=['female', 'male'], out_file=None)
    dtc_graph = graph_from_dot_data(dtc_data)
    dtc_graph.write_png('../images/dtc.png') #export for later usage

def return_accuracies():
    print("DecisionTreeClassifier Accuracy: {}%".format(dtc.score(X_test,y_test)*100))
    print("RandomTreeClassifier Accuracy: {}%".format(rtc.score(X_test,y_test)*100))
    print("AdaBoostClassifier Accuracy: {}%".format(abc.score(X_test,y_test)*100))
    print("SVM (RBF) Accuracy: {}%".format(svc.score(X_test,y_test)*100))
    print("LinearSVC Accuracy: {}%".format(lsvc.score(X_test,y_test)*100))
    print("Hard Voting Accuracy: {}%".format(eclf.score(X_test,y_test)*100))

# generate_images()
# print(rtc.feature_importances_)
return_accuracies()
# process_samples()
