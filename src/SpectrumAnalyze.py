#python doesn't have adequete packages for this
#adapted from the original analysis source code https://github.com/RufoJ/Specan/blob/master/seacan3adapted.R
import rpy2.robjects.numpy2ri as rpyn
import rpy2.robjects as robjects
from rpy2.rinterface import NA_Real
from rpy2.robjects.packages import importr
from statistics import mean

def SpectrumAnalyze(filename):
    tuneR = importr("tuneR")
    seewave = importr("seewave")
    r = robjects.r #basically, R prompt

    file = tuneR.readWave(filename)
    songspec = seewave.spec(file, f = file.slots['samp.rate'], plot=0)
    analysis = seewave.specprop(songspec, f=file.slots['samp.rate'], flim = r('c(0, 280/1000)'), plot=0)
    analysis = dict(zip(analysis.names, list(analysis))) #easier access as a python dict
    fundamental = tuple(seewave.fund(file, f=file.slots['samp.rate'], ovlp = 50, \
        threshold = 5, fmax = 280, ylim=r('c(0, 280/1000)'), plot=0, wl = 2048).rx(True,2))
    fundamental = [y for y in fundamental if y is not NA_Real]

    dom = tuple(seewave.fund(file, f=file.slots['samp.rate'], ovlp = 0, threshold = 5, fmax = 280, \
        ylim=r('c(0, 280/1000)'), plot=0, wl = 2048, bandpass = r('c(0,22) * 1000'), fftw = 1).rx(True,2))
    dom = [y for y in dom if y is not NA_Real]

    #Breakdown each variable into the same format as the original voice.csv
    meanfreq = analysis['mean'][0]/1000
    sd =  analysis['sd'][0]/1000
    median = analysis['median'][0]/1000
    Q25 = analysis['Q25'][0]/1000
    Q75 = analysis['Q75'][0]/1000
    IQR = analysis['IQR'][0]/1000
    skew = analysis['skewness'][0]
    kurt = analysis['kurtosis'][0]
    sp_ent = analysis['sh'][0]
    sfm = analysis['sfm'][0]
    mode = analysis['mode'][0]/1000
    centroid = analysis['cent'][0]/1000
    meanfun =  mean(fundamental)
    minfun = min(fundamental)
    maxfun = max(fundamental)
    meandom = mean(dom)
    mindom = min(dom)
    maxdom = max(dom)
    dfrange = (maxdom-mindom)

    changes = []
    for i in range(0, len(dom)):
        if i >= len(dom):
            dom2 = dom[i+1]
        else:
            dom2 = 0

        change = abs(dom[i] - dom2)
        changes.append(change)

    if (mindom == maxdom):
        modindx = 0
    else:
        modindx = mean(changes)/dfrange

    return [meanfreq, sd, median, Q25, Q75, IQR, skew, kurt, sp_ent, sfm, mode,
        centroid, meanfun, minfun, maxfun, meandom, mindom, maxdom, dfrange, modindx]
