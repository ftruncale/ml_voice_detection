\documentclass{article}
\usepackage[utf8x]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage{mathtools,setspace,indentfirst,graphicx,float,url,color}
\usepackage{fancyhdr} % for header
\usepackage{listings}
\usepackage[english]{babel}

\doublespacing{}

\fancyhf{}
\pagestyle{fancy}

\lhead{F Truncale}
\chead{\thepage}
\rhead{Spring 2018}
\renewcommand{\headrulewidth}{0pt} % remove the horizontal line along the header

\lstset{%
  basicstyle=\ttfamily,
  captionpos=b,
  frame=tb,
  tabsize=2,
  showstringspaces=false,
  commentstyle=\color[RGB]{24,135,64},
  keywordstyle=\color{blue},
  stringstyle=\color{red}
}

\begin{document}

\thispagestyle{empty}

\begin{titlepage}
  \vspace*{\fill}
  \begin{center}
    {\Huge Recognizing Gender by Voice Sample}\\[0.4cm]
    {\large by Francesca Truncale}\\[1cm]
    {\large Introduction to Machine Learning}\\
    {\large Professor Erik Grimmelmann}\\
    {\large Spring 2018}
  \end{center}
  \vspace*{\fill}
\end{titlepage}

\pagenumbering{roman}
%table of contents
\tableofcontents

\newpage

\pagenumbering{arabic}
\setcounter{page}{1}

%\addcontentsline{toc}{section}{Introduction}
\section{Introduction}

The goal of this project is to determine if the gender of a speaker can be identified by a voice sample.

  \subsection{Data Set}

  The dataset (\texttt{voice.csv}) contains 3170 rows, and was found at \url{https://www.kaggle.com/primaryobjects/voicegender}.
  There are 20 features in this dataset, which are mostly various calculations of an audio sample's frequency. The non-frequency based data is based on the sample's \textit{spectral} properties.

  For example, the spectral flatness of an audio sample is a measure of how noise-like a sound is, rather than tone-like. The centroid is calculated from the weighted mean of frequencies in the sample, but can be better attributed to the ``brightness'' of a sound.

  A complete listing of features is below:
  \begin{itemize}
    \item Mean frequency (kHz)
    \item Standard deviation of frequency
    \item Median frequency
    \item First quantile
    \item Third Quantile
    \item Interquantile range
    \item Skew
    \item Kurtosis
    \item Spectral entropy
    \item Spectral flatness
    \item Mode frequency
    \item Frequency centroid
    \item Peak frequency
    \item Average of fundamental frequency measured across acoustic signal
    \item Minimum of fundamental frequency measured across acoustic signal
    \item Maximum of fundamental frequency measured across acoustic signal
    \item Average of dominant frequency measured across acoustic signal
    \item Minimum of dominant frequency measured across acoustic signal
    \item Maximum of dominant frequency measured across acoustic signal
    \item Range of dominant frequency measured across acoustic signal
    \item Modulation index
  \end{itemize}

\section{Approaches Considered}

  \subsection{Linear Classifiers}

  Linear classifiers were quickly removed from consideration after plotting each pair of features.

  \begin{figure}[H]
    \centering
    \includegraphics[scale=0.15]{../images/test.png}
    \caption{Plotted pairs of all features using seaborn.}
  \end{figure}

  Clearly, there is significantly overlap for all pairs of features, making a linear classifier model the least optimal choice.

  \subsection{Support Vector Machine}
    Due to the features appearing not to be linearly unseparable, a support vector machine was the first choice of model to use. The \texttt{poly} and \texttt{rbf} kernels were chosen for testing, as they are meant for non-linear separable cases.

    Features were selected using the \texttt{feature\_importances\_} variable of a trained \texttt{RandomForestClassifier}.

    \begin{figure}[H]
      \centering
      \includegraphics[scale=1]{../images/feature_importances.png}
      \caption{Output from \texttt{feature\_importances} variable.}
    \end{figure}

    As shown from the output, it appears that minimum and maximum dominant frequency (\texttt{mindom/maxdom}) are the least important features, while the average fundamental frequency (\texttt{meanfun}) is the most important.

    The \texttt{rbf} function, when using the top four important features (average fundamental frequency, standard deviation, overall frequency in the first quarter, and interquantile range)  has an accuracy of ~92\%. This is an improvement over using all features, which results in an accuracy of 73\%.\\

      \subsubsection{LinearSVC}
        When using a linear support vector classifier with the same features, accuracy is 96\%.

  \subsection{Decision Tree Classifier}
    Due to the difficulty in feature selection and lackluster performance of the non-linear SVM, a decision tree classifier was selected.

    \begin{figure}[H]
      \centering
      \includegraphics[scale=0.12]{../images/dtc.png}
      \caption{Decision tree classifier visualization.}
    \end{figure}

    Using the ``entropy'' criteria for the decision tree, an accuracy of 96\% is achieved when not limiting the depth.

      \subsubsection{AdaBoost Classifier}
        In order further increase the accuracy for this dataset, an AdaBoost classifier was tested as well, using Decision Trees as the base model. As expected, AdaBoost had a minor boost in accuracy to ~97.6\%.

      \subsubsection{Random Forest Classifier}
        Similarly, a random forest classifier increased accuracy to ~97.6\%.

    \subsection{Hard Voting Classifier}
      With the Decision Tree classifier being capped at ~97\% accuracy, a hard voting classifier was chosen in order to increase accuracy further. This classifier was set to use the Random Forest, AdaBoost, and LinearSVC as they had the best accuracy scores in testing.

      However, it is important to note that in order to use these together, they each need to use the same number of features. As a result, the LinearSVC classifier has to use all features, which brings its accuracy down to 93\%.

      The overall accuracy of the hard voting classifier is ~98\%.

\section{Frontend GUI}
  A frontend GUI was developed in PyQT5 in order to demonstrate the model, as well as check individual audio samples.

  \begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{../images/gui.png}
    \includegraphics[scale=0.5]{../images/gui_content.png}
    \caption{PyQT5 written GUI for demonstration purposes.}
  \end{figure}

  After selecting a wav file, the sample is processed and then checked against the model with the \texttt{predict} member function. The result is then added to the integrated table.

\section{Planned Project Expansion}
  As expected, the dataset did not account for non ``typical'' voices, such as those of transgender and nonbinary people. As a result, this model is not capable of correctly placing those who fall those categories.

  In an attempt to solve this issue, the dataset was expanded using voice samples collected from volunteers. A python script to analyze the audio samples was adapted from the original R code used for the original dataset.

  \subsection{Accuracy Issues}
    Due to the samples needing to be collected manually before processing, as well as a lack of sufficient volunteers, there were not enough entries to match the original dataset (1585 entries per class). Attempting to insert the small number of processed samples only served to lower the overall accuracy to 97\%.

    An effort was made to procure voice samples from Reddit communities; however, permission was not granted to gather the samples. As a result, without a sufficient number of volunteers it was not possible to create a proper dataset to expand the project as hoped.

  \section{Conclusion}
    It does seem possible to classify a voice sample by gender, provided it is only the classical male/female pair. An accuracy of 98\% was achieved using the final ensemble classifier for this criteria. It may be possible to increase the accuracy further with more fine-tuned classifiers.

\section{Appendix}
  \subsection{Python Code}
    \subsubsection{EnsembleClassifier Class}
      \lstinputlisting[language=python,caption={python ensemble classifier class.}]{../src/machine.py}
    \subsubsection{Analysis}
      \lstinputlisting[language=python,caption={Python function to analyze voice samples and return a list of values to match the dataset}]{../src/analysis.py}
    \subsubsection{GUI}
      \lstinputlisting[language=python,caption={pyQT5 GUI for demonstration purposes.}]{../src/gui.py}
    \subsubsection{Project Utilities}
      \lstinputlisting[language=python,caption={A python script to create diagrams, test different models on the data.}]{../src/project_utilities.py}

\end{document}
